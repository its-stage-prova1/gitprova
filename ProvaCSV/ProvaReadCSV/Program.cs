﻿using CsvHelper;
using CsvHelper.Configuration;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
namespace ProvaReadCSV
{
    internal class Program
    {
        //Oggetto per log4net
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
    
        static void Main(string[] args)
        {
            string FileOrganizzativeGruppo = "UnitaOrganizzative_Gruppo.csv";
            ProveCSV<UO>(FileOrganizzativeGruppo);

            string FileAttributiGruppo = "Attributi_Gruppo.csv";
            ProveCSV<AG>(FileAttributiGruppo);



            //Prove Log4net
            /*
            XmlConfigurator.Configure(new FileInfo("log4net.config"));

            log.Debug("Debug message");
            log.Info("Info message");
            log.Warn("Warning message");
            log.Error("Error message");
            log.Fatal("Fatal error message");
            */

            //Attendo la lettura di un tasto per non far chiudere il terminale
            Console.ReadLine();
        }


        static string FindProjectDirectory()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            while (!File.Exists(Path.Combine(currentDirectory, "ProvaReadCSV.csproj")))
            {
                currentDirectory = Directory.GetParent(currentDirectory)?.FullName;

                if (currentDirectory == null)
                {
                    throw new InvalidOperationException("File .csproj non trovato nel percorso del progetto.");
                }
            }

            return currentDirectory;
        }

        private static void ProveCSV<T>(string nameFile)
        {
            List<T> CSVRead = ReadCSV<T>(nameFile);
            PrintListCount<T>(CSVRead);
        }

        private static void PrintListCount<T>(List<T> InputList)
        {
            Console.WriteLine($"{InputList.Count}");
        }

        private static List<T> ReadCSV<T>(string nameFile)
        {
            string path = FindProjectDirectory();
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ";",
                BadDataFound = null
            };
            using (var reader = new StreamReader($"{path}\\CSVFile\\{nameFile}"))
            using (var csv = new CsvReader(reader, config))
            {
                {
                    return csv.GetRecords<T>().ToList<T>();
                }
            }
        }
    }
}
