﻿using CsvHelper.Configuration.Attributes;

public class UO
{
    [Name("Istituto")]
    public string Istituto { get; set; }

    [Name("Id")]
    public string Id { get; set; }

    [Name("Id padre")]
    public string IdPadre { get; set; }

    [Name("Stato")]
    public string Stato { get; set; }

    [Name("Descrizione")]
    public string Descrizione { get; set; }

    [Name("Descrizione breve")]
    public string DescrizioneBreve { get; set; }

    [Name("Codice Categoria")]
    public string CodiceCategoria { get; set; }

    [Name("Categoria")]
    public string Categoria { get; set; }

    [Name("Codice Livello")]
    public string CodiceLivello { get; set; }

    [Name("Livello")]
    public string Livello { get; set; }

    [Name("Data Apertura")]
    public string DataApertura { get; set; }

    [Name("Data Chiusura")]
    public string DataChiusura { get; set; }

    [Name("CAB")]
    public string CAB { get; set; }

    [Name("Data Patrono")]
    public string DataPatrono { get; set; }

    [Name("Patrono")]
    public string Patrono { get; set; }

    [Name("Codice Nazione")]
    public string CodiceNazione { get; set; }

    [Name("Nazione")]
    public string Nazione { get; set; }

    [Name("Codice Regione")]
    public string CodiceRegione { get; set; }

    [Name("Regione")]
    public string Regione { get; set; }

    [Name("Codice Provincia")]
    public string CodiceProvincia { get; set; }

    [Name("Provincia")]
    public string Provincia { get; set; }

    [Name("Codice Città")]
    public string CodiceCitta { get; set; }

    [Name("Città")]
    public string Citta { get; set; }

    [Name("Frazione")]
    public string Frazione { get; set; }

    [Name("CAP")]
    public string CAP { get; set; }

    [Name("Indirizzo")]
    public string Indirizzo { get; set; }

    [Name("Palazzo")]
    public string Palazzo { get; set; }

    [Name("Piano")]
    public string Piano { get; set; }

    [Name("Interno")]
    public string Interno { get; set; }

    [Name("Approssimazione GPS")]
    public string ApprossimazioneGPS { get; set; }

    [Name("Longitudine")]
    public string Longitudine { get; set; }

    [Name("Latitudine")]
    public string Latitudine { get; set; }

    [Name("Numero ATM")]
    public string NumeroATM { get; set; }

    [Name("Numero ATM evoluti")]
    public string NumeroATMEvoluti { get; set; }
}
