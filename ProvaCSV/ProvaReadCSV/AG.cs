﻿using CsvHelper.Configuration.Attributes;

namespace ProvaReadCSV
{
    internal class AG
    {
        [Name("Id")]
        public string Id { get; set; }

        [Name("Codice Attributo")]
        public string CodiceAttributo { get; set; }

        [Name("Attributo")]
        public string Attributo { get; set; }

        [Name("Valore")]
        public string Valore { get; set; }

        [Name("Note")]
        public string Note { get; set; }

    }
}
