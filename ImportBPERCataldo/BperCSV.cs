﻿
using CsvHelper;
using CsvHelper.Configuration;
using Prova;
using System.Globalization;

internal class BperCSV
{
    static FileLoggerBPER fileLogger = new FileLoggerBPER(typeof(BperCSV));

    //Path da modificare se cambia la posizione dei file CSV
    string path = "C:\\Users\\U-24\\Downloads\\#12574 UO BPER\\";

    //Nome dei file
    string PathFileUO = "UnitaOrganizzative_Gruppo.csv";
    string PathFileAttributi = "Attributi_Gruppo.csv";

    //Nome dei file .bad da generare in caso si rilevino dei problemi
    string PathFileUOBad = "UnitaOrganizzative_Gruppo.bad";
    string PathFileAttributiBad = "Attributi_Gruppo.bad";


    //Configurazione del file CSV
    CsvConfiguration config = new CsvConfiguration(CultureInfo.InvariantCulture)
    {
        HasHeaderRecord = false,
        Delimiter = ";",
        BadDataFound = null
    };

    public List<UO> listUO { get; set; }
    public List<AG> listAG { get; set; }
    public List<UO> listUOBadRecords { get; set; }
    public List<AG> listAGBadRecords { get; set; }

    public BperCSV() { }

    #region ReadUOFile
    internal void ReadUOFile()
    {
        using (var reader = new StreamReader($"{path}\\{PathFileUO}"))
        using (var csv = new CsvReader(reader, config))
        {
            try
            {
                {
                    fileLogger.log.Error($"{PathFileUO} letto correttamente");
                    listUO = csv.GetRecords<UO>().ToList<UO>();
                }
            }
            catch (CsvHelperException ex)
            {
                fileLogger.log.Info($"OK!");
                fileLogger.log.Error($"{ex.Message}");
            }
        }
    }
    #endregion

    # region ReadAGFile
    internal void ReadAGFile()
    {
        using (var reader = new StreamReader($"{path}\\{PathFileAttributi}"))
        using (var csv = new CsvReader(reader, config))
        {
            try
            {
                {
                    fileLogger.log.Error($"{PathFileAttributi} letto correttamente");
                    listAG = csv.GetRecords<AG>().ToList<AG>();
                }
            }
            catch (CsvHelperException ex)
            {
                fileLogger.log.Error($"{ex.Message}");
            }
        }
    }
    #endregion

    #region WriteUOBadFIle

    internal void WriteUOBadFIle(List<UO> badRecords)
    {
        using (var writer = new StreamWriter($"{path}\\{PathFileUOBad}"))
        using (var csv = new CsvWriter(writer, config))
        {
            // Scrivere i dati nella destinazione
            csv.WriteRecords(badRecords);
        }
    }
    #endregion

    #region WriteAGBadFile

    internal void WriteAGBadFile(List<AG> badRecords)
    {
        using (var writer = new StreamWriter($"{path}\\{PathFileAttributiBad}"))
        using (var csv = new CsvWriter(writer, config))
        {
            // Scrivere i dati nella destinazione
            csv.WriteRecords(badRecords);
        }
    }
    #endregion

    #region RenameFile
    internal void RenameFile()
    {
        DateTime date = DateTime.Now;
        string formate = $"{date.Year}{date.Month}{date.Day}_{date.Hour}{date.Minute}{date.Second}_";

    }
    #endregion

    #region CheckBadRecordUO
    internal void CheckBadRecordUO()
    {
        int counter = 0;
        foreach (UO item in listUO)
        {
            if (item.IdIsNull())
            {
                fileLogger.log.Error($"BAD RECORD : {item.Id} id non valorizzato");

            }
            else if (item.IstitutoIsNull())
            {
                fileLogger.log.Error($"BAD RECORD : {item.Id} istituto non valorizzato");
            }
            else
            {
                counter++;
            }
        }
        fileLogger.log.Info($"Sono stati inseriti {counter} record");
    }
    #endregion

    #region CheckBadRecordAG
    internal void CheckBadRecordAG()
    {
        int counter = 0;
        foreach (AG item in listAG)
        {
            if (item.IdIsNull())
            {
                fileLogger.log.Error($"BAD RECORD : {item.Id} id non valorizzato");
            }
            else if (item.CodAttIsNull())
            {
                fileLogger.log.Error($"BAD RECORD : {item.Id} codice attributo non valorizzato");
            }
            else
            {
                counter++;
            }
        }
        fileLogger.log.Info($"Sono stati inseriti {counter} record");
    }
    #endregion

}