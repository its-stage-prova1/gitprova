﻿

namespace Prova
{
    public class ImportBPER
    {
        BperCSV bperCSV;
        OracleDBBper oracleDB;

        public ImportBPER()
        {
            bperCSV = new BperCSV();
            oracleDB = new OracleDBBper();
        }

        internal void SvuotaTabelle()
        {
            oracleDB.Delete();
        }

        internal void LoadData()
        {
            oracleDB.InsertUO(bperCSV.listUO);
            oracleDB.InsertAG(bperCSV.listAG);
        }

        internal void PrintCSVRecord()
        {
            Console.WriteLine(bperCSV.listUO.Count);
            Console.WriteLine(bperCSV.listAG.Count);
        }

        internal void ReadFIleCSV()
        {
            bperCSV.ReadUOFile();
            bperCSV.ReadAGFile();
            bperCSV.CheckBadRecordUO();
            bperCSV.CheckBadRecordAG();
        }

        internal void GetDT()
        {
            oracleDB.GetUO<DT>("A", "SI_COD_AREA", "117");
        }

        internal void GetAree()
        {
            oracleDB.GetUO<DT>("A", "SI_COD_AREA", "150");
        }
        internal void GetFiliali()
        {
            oracleDB.GetUO<DT>("A", "CONTR_ACC", "190,180,200,210");
        }
    }
}
