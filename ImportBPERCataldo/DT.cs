﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Prova
{
    internal class DT
    {
        [Column("ISTITUTO")]
        public string Istituto { get; set; }

        [Column("ID")]
        public string Id { get; set; }

        [Column("IDPADRE")]
        public string IdPadre { get; set; }

        [Column("STATO")]
        public string Stato { get; set; }

        [Column("DESCRIZIONE")]
        public string Descrizione { get; set; }

        [Column("DESCRIZIONEBREVE")]
        public string DescrizioneBreve { get; set; }

        [Column("CODICECATEGORIA")]
        public string CodiceCategoria { get; set; }

        [Column("CATEGORIA")]
        public string Categoria { get; set; }

        [Column("CODICELIVELLO")]
        public string CodiceLivello { get; set; }

        [Column("LIVELLO")]
        public string Livello { get; set; }

        [Column("DATAAPERTURA")]
        public string DataApertura { get; set; }

        [Column("DATACHIUSURA")]
        public string DataChiusura { get; set; }

        [Column("CAB")]
        public string CAB { get; set; }

        [Column("DATAPATRONO")]
        public string DataPatrono { get; set; }

        [Column("PATRONO")]
        public string Patrono { get; set; }

        [Column("CODICENAZIONE")]
        public string CodiceNazione { get; set; }

        [Column("NAZIONE")]
        public string Nazione { get; set; }

        [Column("CODICEREGIONE")]
        public string CodiceRegione { get; set; }

        [Column("REGIONE")]
        public string Regione { get; set; }

        [Column("CODICEPROVINCIA")]
        public string CodiceProvincia { get; set; }

        [Column("PROVINCIA")]
        public string Provincia { get; set; }

        [Column("CODICECITTA")]
        public string CodiceCitta { get; set; }

        [Column("CITTA")]
        public string Citta { get; set; }

        [Column("FRAZIONE")]
        public string Frazione { get; set; }

        [Column("CAP")]
        public string CAP { get; set; }

        [Column("INDIRIZZO")]
        public string Indirizzo { get; set; }

        [Column("PALAZZO")]
        public string Palazzo { get; set; }

        [Column("PIANO")]
        public string Piano { get; set; }

        [Column("INTERNO")]
        public string Interno { get; set; }

        [Column("ID")]
        public string IdAtt { get; set; }

        [Column("CODICEATTRIBUTO")]
        public string CodiceAttributo { get; set; }

        [Column("ATTRIBUTO")]
        public string Attributo { get; set; }

        [Column("VALORE")]
        public string Valore { get; set; }

        [Column("NOTE")]
        public string Note { get; set; }
    }
}