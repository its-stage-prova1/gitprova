﻿using CsvHelper.Configuration.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

[Table("UNITAORG_ATTRIBUTI_GRUPPO")]
public class AG
{


    [Name("Id")]
    [Column("ID")]
    public string Id { get; set; }

    [Name("Codice Attributo")]
    [Column("CODICEATTRIBUTO")]
    public string CodiceAttributo { get; set; }

    [Name("Attributo")]
    [Column("ATTRIBUTO")]
    public string Attributo { get; set; }

    [Name("Valore")]
    [Column("VALORE")]
    public string Valore { get; set; }

    [Name("Note")]
    [Column("NOTE")]
    public string Note { get; set; }

    public bool IdIsNull()
    {
        return Id == null;
    }
    public bool CodAttIsNull()
    {
        return CodiceAttributo == null;
    }

    internal bool IsBadRecords()
    {
        return this.IdIsNull() && this.CodAttIsNull();
    }

    internal bool IsComplate()
    {
        throw new NotImplementedException();
    }
}
