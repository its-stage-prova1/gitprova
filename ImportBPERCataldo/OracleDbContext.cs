﻿using Microsoft.EntityFrameworkCore;

public class OracleDbContext : DbContext
{
    public DbSet<UO> UnitaOrganizzativeGruppo { get; set; }
    public DbSet<AG> AttributiGruppo { get; set; }

    public OracleDbContext(DbContextOptions<OracleDbContext> dbContextOptions) : base(dbContextOptions)
    {

    }
}