﻿using CsvHelper.Configuration.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

[Table("UNITAORG_UO_GRUPPO")]
public class UO
{

    public UO() { }

    [Name("Istituto")]
    [Column("ISTITUTO")]
    public string Istituto { get; set; }

    [Name("Id")]
    [Column("ID")]
    public string Id { get; set; }

    [Name("Id padre")]
    [Column("IDPADRE")]
    public string IdPadre { get; set; }

    [Name("Stato")]
    [Column("STATO")]
    public string Stato { get; set; }

    [Name("Descrizione")]
    [Column("DESCRIZIONE")]
    public string Descrizione { get; set; }

    [Name("Descrizione breve")]
    [Column("DESCRIZIONEBREVE")]
    public string DescrizioneBreve { get; set; }

    [Name("Codice Categoria")]
    [Column("CODICECATEGORIA")]
    public string CodiceCategoria { get; set; }

    [Name("Categoria")]
    [Column("CATEGORIA")]
    public string Categoria { get; set; }

    [Name("Codice Livello")]
    [Column("CODICELIVELLO")]
    public string CodiceLivello { get; set; }

    [Name("Livello")]
    [Column("LIVELLO")]
    public string Livello { get; set; }

    [Name("Data Apertura")]
    [Column("DATAAPERTURA")]
    public string DataApertura { get; set; }

    [Name("Data Chiusura")]
    [Column("DATACHIUSURA")]
    public string DataChiusura { get; set; }

    [Name("CAB")]
    [Column("CAB")]
    public string CAB { get; set; }

    [Name("Data Patrono")]
    [Column("DATAPATRONO")]
    public string DataPatrono { get; set; }

    [Name("Patrono")]
    [Column("PATRONO")]
    public string Patrono { get; set; }

    [Name("Codice Nazione")]
    [Column("CODICENAZIONE")]
    public string CodiceNazione { get; set; }

    [Name("Nazione")]
    [Column("NAZIONE")]
    public string Nazione { get; set; }

    [Name("Codice Regione")]
    [Column("CODICEREGIONE")]
    public string CodiceRegione { get; set; }

    [Name("Regione")]
    [Column("REGIONE")]
    public string Regione { get; set; }

    [Name("Codice Provincia")]
    [Column("CODICEPROVINCIA")]
    public string CodiceProvincia { get; set; }

    [Name("Provincia")]
    [Column("PROVINCIA")]
    public string Provincia { get; set; }

    [Name("Codice Città")]
    [Column("CODICECITTA")]
    public string CodiceCitta { get; set; }

    [Name("Città")]
    [Column("CITTA")]
    public string Citta { get; set; }

    [Name("Frazione")]
    [Column("FRAZIONE")]
    public string Frazione { get; set; }

    [Name("CAP")]
    [Column("CAP")]
    public string CAP { get; set; }

    [Name("Indirizzo")]
    [Column("INDIRIZZO")]
    public string Indirizzo { get; set; }

    [Name("Palazzo")]
    [Column("PALAZZO")]
    public string Palazzo { get; set; }

    [Name("Piano")]
    [Column("PIANO")]
    public string Piano { get; set; }

    [Name("Interno")]
    [Column("INTERNO")]
    public string Interno { get; set; }

    [Name("Approssimazione GPS")]
    [NotMapped]
    public string ApprossimazioneGPS { get; set; }

    [Name("Longitudine")]
    [NotMapped]
    public string Longitudine { get; set; }

    [Name("Latitudine")]
    [NotMapped]
    public string Latitudine { get; set; }

    [Name("Numero ATM")]
    [NotMapped]
    public string NumeroATM { get; set; }

    [Name("Numero ATM evoluti")]
    [NotMapped]
    public string NumeroATMEvoluti { get; set; }

    public bool IdIsNull()
    {
        return Id == null;
    }
    public bool IstitutoIsNull()
    {
        return Istituto == null;
    }
}
