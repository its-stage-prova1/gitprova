﻿using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;

namespace Prova
{
    public class OracleDBBper
    {

        FileLoggerBPER fileLogger = new FileLoggerBPER(typeof(OracleDBBper));

        readonly string _connectionStringOracle = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1522))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=XE)));User Id=ESERCIZI;Password=ESERCIZI;";

        public OracleDBBper() { }

        #region InsertUO
        public void InsertUO(List<UO> listUO)
        {
            var dbContextOptions = new DbContextOptionsBuilder<OracleDbContext>()
                .UseOracle(_connectionStringOracle)
                .Options;

            using (var dbContext = new OracleDbContext(dbContextOptions))
            {
                try
                {
                    foreach (UO UOItemList in listUO)
                    {
                        try
                        {
                            dbContext.UnitaOrganizzativeGruppo.AddAsync(UOItemList);
                        }
                        catch (DbUpdateException dbException)
                        {
                            if (dbException.InnerException is OracleException oracleException)
                            {
                                fileLogger.log.Error($"Oracle Exception {oracleException.Number}: {oracleException.Message}");
                            }
                            else
                            {
                                fileLogger.log.Error($"DbUpdateException: {dbException.Message}");
                            }
                        }
                    }
                    dbContext.SaveChangesAsync();
                }
                catch (Exception exception)
                {
                    fileLogger.log.Error(exception.Message);
                }
            }
            Console.WriteLine("Fatto!!!");
        }
        #endregion

        #region InsertAG
        public void InsertAG(List<AG> listAG)
        {
            int badRecordsCount = 0;
            var dbContextOptions = new DbContextOptionsBuilder<OracleDbContext>()
                .UseOracle(_connectionStringOracle)
                .Options;
            using (var dbContext = new OracleDbContext(dbContextOptions))
            {
                try
                {
                    foreach (AG AGItemList in listAG)
                    {
                        do
                        {
                            try
                            {
                                dbContext.AttributiGruppo.AddAsync(AGItemList);
                            }
                            catch (DbUpdateException dbException)
                            {
                                badRecordsCount++;
                                if (dbException.InnerException is OracleException oracleException)
                                {
                                    fileLogger.log.Error($"Oracle Exception {oracleException.Number}: {oracleException.Message}");
                                    Console.WriteLine();
                                }
                                else
                                {
                                    fileLogger.log.Error($"DbUpdateException: {dbException.Message}");
                                }
                            }
                        } while (badRecordsCount < 2);
                    }
                    dbContext.SaveChangesAsync();
                }
                catch (Exception exception)
                {
                    fileLogger.log.Error(exception.Message);
                }
                dbContext.SaveChanges();
            }
            fileLogger.log.Error("Fatto!!!");
        }
        #endregion

        internal void DeleteUO()
        {
            var dbContextOptions = new DbContextOptionsBuilder<OracleDbContext>()
                .UseOracle(_connectionStringOracle)
                .Options;
            using (var dbContext = new OracleDbContext(dbContextOptions))
            {
                dbContext.UnitaOrganizzativeGruppo.ExecuteDeleteAsync();
                dbContext.SaveChangesAsync();
            }
        }
        internal void DeleteAG()
        {
            var dbContextOptions = new DbContextOptionsBuilder<OracleDbContext>()
                .UseOracle(_connectionStringOracle)
                .Options;
            using (var dbContext = new OracleDbContext(dbContextOptions))
            {
                dbContext.AttributiGruppo.ExecuteDeleteAsync();
                dbContext.SaveChangesAsync();
            }
        }
        internal void Delete()
        {
            DeleteUO();
            DeleteAG();
        }

        internal void GetUO<DT>(string stato, string codLiv, string codAtt)
        {
            var dbContextOptions = new DbContextOptionsBuilder<OracleDbContext>()
                .UseOracle(_connectionStringOracle)
                .Options;
            using (var dbContext = new OracleDbContext(dbContextOptions))
            {
                /*
 var query_DT = from u in dbContext.UnitaOrganizzativeGruppo
                join a in dbContext.AttributiGruppo on u.Id equals a.Id
                where u.Stato == stato &&
                u.CodiceLivello == codLiv &&
                a.CodiceAttributo == codAtt
                 * select new DT()
                {
                    Id = u.Id,
                    IdPadre = u.IdPadre,
                };
 List<DT> results = query_DT.ToList();
                */
            }
        }
    }
}
