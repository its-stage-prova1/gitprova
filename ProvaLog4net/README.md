# LOG4NET

## Installa il pacchetto NuGet log4net

- Progetto > Gestisci pacchetti NuGet...
- Sfoglia
- Cerca "log4net" e installa

## Crea il file log4net.config

- bin > Debug
- crea un file
- rinomina log4net.config

## Configurare il file log4net.config

```xml
<?xml version="1.0" encoding="utf-8" ?>
<log4net>
  <appender name="RollingFileAppender" type="log4net.Appender.RollingFileAppender">
    <file value=".\log\fileLog.log" />
    <appendToFile value="true" />
    <rollingStyle value="Date" />
    <datePattern value=".yyyy-MM-dd-HH-mm-ss'.log'" />
    <maximumFileSize value="10MB" />
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%date [%thread] %-5level %logger - %message%newline" />
    </layout>
  </appender>

  <root>
    <level value="DEBUG" />
    <appender-ref ref="RollingFileAppender" />
  </root>
</log4net>
```

### Settare i parametri per generare i file e salvare il rolling

- Specifica la path e il nome di memorizzazione del file `file value=".\log\app-log.txt" `
- Imposta se aggiungre o sovrascrivere i file`appendToFile value="false"`
- Specifica che il file verra backuppato per data`rollingStyle value="Date"`
- Indico come rinominare il file vecchio`datePattern value=".yyyy-MM-dd-HH-mm-ss'.log'"`
- `maximumFileSize value="10MB"`

### Dichiaro come voglio salvare

se imposto

```xml
<file value=".\log\fileLog.log" />
<datePattern value=".yyyy-MM-dd-HH-mm-ss'.log'" />
```

il rolling viene cosi

> fileLog.log
> fileLog.log.2024-01-17-12-54-47.log

mentre se imposto

```xml
<file value=".\log\fileLog" />
<datePattern value=".yyyy-MM-dd-HH-mm-ss'.log'" />
```

il rolling viene cosi

```
log
|--fileLog
|--fileLog.2024-01-17-12-54-47.log
```

## Creare una classe C#

Creo una classe che si occuera di effettuare tutte le scritture sul file log

```cs
using log4net;
using System.IO;

namespace ProveLog4Net
{
    internal class FileLogger
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        public FileLogger()
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.config"));
        }
    }
}
```

## Uso la classe FileLogger

```cs
FileLogger fileLogger = new FileLogger();


fileLogger.Warn("Warning Message");
fileLogger.Info("Info Message");
fileLogger.Error("Error Message");
fileLogger.Debug("Debug Message");
fileLogger.Fatal("Fatal Message");
```


C:\Users\U-24\source\repos\ImportBPER\ImportBPER.sln

C:\Users\U-24\source\repos\ImportBPER\ImportBPER.csproj



```sql
CREATE TABLE TSEFAAREABPER AS (
  select
      u.istituto C_BANCA_BPER, to_number('10' || lpad(u.id, 10, '0')),
      10 k_tipo_uo,
      u.id c_uo_bper,
      u.DESCRIZIONEBREVE x_desc,
      0 F_VALIDO,
      99999 k_anag_uo_padre
  from
      UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'SI_COD_AREA'
      JOIN UNITAORG_EXTRAINFO ue ON to_number(ue.C_ABI) = to_number(u.ISTITUTO) and ue.C_UO_BPER = a.valore
  where
      u.stato in ('A') and u.codicelivello in (117)
      AND u.istituto IN (5387, 1015)
  UNION -- dt KC (fittizia)
      select
          '5387' C_BANCA_BPER,
          to_number('10' || lpad('7111', 10, '0')) k_anag_uo,
          10 k_tipo_uo,
          '0' c_uo_bper,
          'DR FIL. KC IMP.' x_desc,
          0 F_VALIDO,
          99999 k_anag_uo_padre
      from
          dual
  UNION -- dt Consulenti (fittizia)
      select
          '5387' C_BANCA_BPER,
          to_number('10' || lpad('7113', 10, '0')) k_anag_uo,
          10 k_tipo_uo, '0' c_uo_bper,
          'DR FIL. CONSULENTI FIN.' x_desc, 0 F_VALIDO,
          99999 k_anag_uo_padre
      from
          dual
  union
    select
        u.istituto C_BANCA_BPER,
        to_number('5' || lpad(u.id, 10, '0')) k_anag_uo,
        5 k_tipo_uo,
        u.id c_uo_bper,
        u.DESCRIZIONEBREVE x_desc,
        0 F_VALIDO,
        -1*to_number(u.idpadre) k_anag_uo_padre -- padre da aggiornare dopo
    from
        UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'SI_COD_AREA'
    where
        u.stato in ('A') and u.codicelivello in (150)
        AND u.istituto IN (5387, 1015)
  UNION -- mercati bper
      select
          u.istituto C_BANCA_BPER,
          to_number('5' || lpad(u.id, 10, '0')) k_anag_uo,
          5 k_tipo_uo,
          u.id c_uo_bper,
          u.DESCRIZIONEBREVE x_desc,
          0 F_VALIDO,
          -1*to_number(u.idpadre) k_anag_uo_padre -- padre da aggiornare dopo
      from UNITAORG_UO_GRUPPO u
      where u.stato in ('A') and u.CODICELIVELLO = 220 AND u.descrizione LIKE '%UFF. MERCATO IMPRESE%' AND u.istituto IN (5387, 1015)
  UNION -- mercati sardegna (fittizi)
      select
          u.istituto C_BANCA_BPER,
          to_number('5' || lpad(u.id, 10, '0')) k_anag_uo,
          5 k_tipo_uo,
          '0' c_uo_bper,
          'AT ' || u.DESCRIZIONEBREVE x_desc,
          0 F_VALIDO,
          -1*to_number(u.idpadre) k_anag_uo_padre -- padre da aggiornare dopo
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'CONTR_ACC'
          join UNITAORG_ATTRIBUTI_GRUPPO aa on aa.id=u.id AND aa.codiceattributo = 'TIPO_SPORTELLO' AND aa.valore = 'F'
      where
          u.stato in ('A') and codicelivello in (190,180,200,210)
          AND ISTITUTO = 1015
  UNION -- aree KC (fittizie)
      select
          u.istituto C_BANCA_BPER,
          to_number('5' || lpad(u.id, 10, '0')) k_anag_uo,
          5 k_tipo_uo,
          '0' c_uo_bper, 'AT ' || u.DESCRIZIONEBREVE x_desc,
          0 F_VALIDO,
          to_number('10' || lpad('7111', 10, '0')) k_anag_uo_padre
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'CONTR_ACC'
      where
          u.stato in ('A') and codicelivello in (190,180,200,210) AND
          u.DESCRIZIONE LIKE 'FILIALE KEY CLIENT%' AND
          u.istituto IN (5387, 1015)
  UNION -- aree Consulenti (fittizie)
      select
          u.istituto C_BANCA_BPER,
          to_number('5' || lpad(u.id, 10, '0')) k_anag_uo,
          5 k_tipo_uo,
          '0' c_uo_bper,
          'AT ' || u.DESCRIZIONEBREVE x_desc,
          0 F_VALIDO,
          to_number('10' || lpad('7113', 10, '0')) k_anag_uo_padre
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'CONTR_ACC'
      where
          u.stato in ('A') and codicelivello in (190,180,200,210)
          AND a.VALORE = '05387024990000000000'
          AND u.istituto IN (5387, 1015)
  union
-- Filiali: C.I. BPER
      select
          u.istituto C_BANCA_BPER,
          to_number('1' || lpad(u.id, 10, '0')) k_anag_uo,
          1 k_tipo_uo,
          u.id c_uo_bper,
          u.DESCRIZIONEBREVE x_desc,
          1 F_VALIDO,
          -1*to_number(u.idpadre) k_anag_uo_padre -- padre da aggiornare dopo
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'CONTR_ACC'
          join UNITAORG_ATTRIBUTI_GRUPPO aa on aa.id=u.id AND aa.codiceattributo = 'TIPO_SPORTELLO' AND aa.valore = 'F'
      where
          u.stato in ('A') and codicelivello in (190,180,210)
          AND u.istituto = 5387
  UNION -- C.I. sardegna (padre fittizio)
      select
          u.istituto C_BANCA_BPER,
          to_number('1' || lpad(u.id, 10, '0')) k_anag_uo,
          1 k_tipo_uo,
          u.id c_uo_bper,
          u.DESCRIZIONEBREVE x_desc,
          1 F_VALIDO,
          to_number('5' || lpad(u.id, 10, '0')) k_anag_uo_padre
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'CONTR_ACC'
          join UNITAORG_ATTRIBUTI_GRUPPO aa on aa.id=u.id AND aa.codiceattributo = 'TIPO_SPORTELLO' AND aa.valore = 'F'
      where
          u.stato in ('A') and codicelivello in (190,180,210)
          AND u.istituto = 1015
  UNION -- KC
      select
          u.istituto C_BANCA_BPER,
          to_number('1' || lpad(u.id, 10, '0')) k_anag_uo,
          1 k_tipo_uo, u.id c_uo_bper,
          u.DESCRIZIONEBREVE x_desc,
          1 F_VALIDO,
          to_number('5' || lpad(u.id, 10, '0')) k_anag_uo_padre
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'CONTR_ACC'
      where
          u.stato in ('A') and codicelivello in (190,180,210)
          AND u.DESCRIZIONE LIKE 'FILIALE KEY CLIENT%'
          AND u.istituto IN (5387, 1015)
  UNION -- Consulenti
      select
          u.istituto C_BANCA_BPER,
          to_number('1' || lpad(u.id, 10, '0')) k_anag_uo,
          1 k_tipo_uo,
          u.id c_uo_bper,
          u.DESCRIZIONEBREVE x_desc,
          1 F_VALIDO,
          to_number('5' || lpad(u.id, 10, '0')) k_anag_uo_padre
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'CONTR_ACC'
      where
          u.stato in ('A') and codicelivello in (190,180,210)
          AND a.VALORE = '05387024990000000000'
          AND u.istituto IN (5387, 1015)
  UNION -- Normali (principali)
      select
          u.istituto C_BANCA_BPER,
          to_number('1' || lpad(u.id, 10, '0')) k_anag_uo,
          1 k_tipo_uo,
          u.id c_uo_bper,
          u.DESCRIZIONEBREVE x_desc,
          1 F_VALIDO,
          -1*to_number(u.idpadre) k_anag_uo_padre -- padre da aggiornare dopo
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id AND a.codiceattributo = 'CONTR_ACC'
      where
          u.stato in ('A') and codicelivello in (190,180,210)
          AND u.istituto IN (5387, 1015)
      	  AND NOT EXISTS (
                SELECT
                    *
                FROM
                    UNITAORG_ATTRIBUTI_GRUPPO aa
                where
                    aa.id=u.id AND aa.codiceattributo = 'TIPO_SPORTELLO'
                    AND aa.valore = 'F'
                )
      	  AND u.DESCRIZIONE NOT LIKE 'FILIALE KEY CLIENT%'
      	  AND a.VALORE <> '05387024990000000000'
/*
  UNION -- Normali (secondarie: padre filiale, nonno area)
      select
          u.istituto C_BANCA_BPER,
          to_number('1' || lpad(u.id, 10, '0')) k_anag_uo,
          1 k_tipo_uo,
          u.id c_uo_bper,
          u.DESCRIZIONEBREVE x_desc,
          1 F_VALIDO,
          (SELECT
              -1*to_number(uu.idpadre)
          FROM
              UNITAORG_UO_GRUPPO uu
          WHERE
              uu.ISTITUTO = u.istituto AND uu.id = u.idpadre
          ) k_anag_uo_padre -- padre da aggiornare dopo
      from
          UNITAORG_UO_GRUPPO u join UNITAORG_ATTRIBUTI_GRUPPO a on a.id=u.id
          AND a.codiceattributo = 'CONTR_ACC'
          AND u.istituto IN (5387, 1015)
      where
          u.stato in ('A') and codicelivello in (200)
*/
);
```
