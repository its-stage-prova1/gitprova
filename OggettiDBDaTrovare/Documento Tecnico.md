- [CAnagrafe](#canagrafe)
- [CCRPrimaInfo](#ccrprimainfo)
- [CCedenti](#ccedenti)
- [CCommon](#ccommon)
- [CFidiFactoring](#cfidifactoring)
- [CGestPropost](#cgestpropost)
- [CHomeFactoring](#chomefactoring)
- [CPlanner](#cplanner)
- [CProvider](#cprovider)
- [DBCom](#dbcom)
- [Oggetti DE](#oggetti-de)

### CAnagrafe

- `CAnagrafe.GetDominioIdEstero()` [DOM_ID_ESTERO]
- `CAnagrafe.caricaFormeSociali()` [TSEFCFORMASOCIALE]
- `CAnagrafe.getNdg()` [TSEFAANAG]
- `CAnagrafe.leggiAnagrafe()` [TSEFAANAG_DELIB]
- `CAnagrafe.salvaIdEstero()` [TSEFAANAG_ID_ESTERO]
- `CAnagrafe.setval()` [TSEFAANAG]

### CCRPrimaInfo

- `CCRPrimaInfo.IsAnagOkPerCrReq()` [IS_ANAG_OK_PER_CR_REQ()]

### CCedenti

- `CCedenti.censimentoDebitore()` []
- `CCedenti.esisteRapportoProspect()` [HF_RAPP_PROSPECT_ELAB,HF_RAPP_PROSPECT]
- `CCedenti.getCedenteProspect()` [TSEFACEDENTI_PROSPECT]
- `CCedenti.getCedenti()` [TSEFACEDENTI]
- `CCedenti.isCedenteProspectAttivato()` [TSEFACEDENTI_PROSPECT,TSEFAPRATICA,SNDPRA0F]
- `CCedenti.leggiAnagDebitore()` [TSEFADEBITORI]
- `CCedenti.salvaCedutoConCedente()` [TSEFAPRATICA,TSEFCEDUTI]
- `CCedenti.salvaRischioRapporto()` [TSEFRISCHIORAPP]
- `CCedenti.updateRapportoInProposta()` [TSEFAPRATICA,TSEFASTATI]

### CCommon

- `CCommon.getNazione()` [TSEFANAZIONI]

### CFidiFactoring

- `CFidiFactoring.FidoInProposta()` [TSEFFIDICEDENTE,TSEFAPLURIMI,TSEFOPEPOOL_CONTI,TSEFCONTI,TSEFFORMETECNICHE]
- `CFidiFactoring.aggiornaControlloCondizioniConto()` [TSEFCCONTRCONF]
- `CFidiFactoring.eliminaDatiCondContiCompleti()` [TSEFCONDCONTI]
- `CFidiFactoring.insertLinkContiCeduto()` [TSEFCONTICEDUTI]
- `CFidiFactoring.leggiProspetto()` [TSEFACONTIPROSPETTI]
- `CFidiFactoring.salvaConto()` [TSEFCONTI]
- `CFidiFactoring.salvaDatiCondConti()` [TSEFCONDCONTI_LOG,TSEFCONDCONTI]
- `CFidiFactoring.salvaFido()` [TSEFFIDICEDENTE,TSEFOPEPOOL_CONTI,TSEFCONTI]
- `CFidiFactoring.salvaFormaTecnica()` [TSEFFORMETECNICHE,TSEFFIDICEDENTE]
- `CFidiFactoring.salvaNoteFormaTecnica()` [TSEFFORMETECNICHE]

### CGestPropost

- `CGestPropost.leggiCommRifNdg()` [COMMRIF,TSEFAUTENTI]
- `CGestPropost.salvaCommRif()` [COMMRIF]

### CHomeFactoring

- `CHomeFactoring.aggRichiesta()` [HF_RICHIESTA]
- `CHomeFactoring.esistePlafondValido()` [DUAL]
- `CHomeFactoring.getCedenteEssere()` [TSEFACEDENTI]
- `CHomeFactoring.getCedenteEsserePerNdg()` [TSEFACEDENTI]
- `CHomeFactoring.getCodiceConto()` [TSEFCONTI,TSEFAPRATICA,TSEFASTATI,SNDPRA0F]
- `CHomeFactoring.getNdgDebAcquirente()` [DUAL]
- `CHomeFactoring.getPraticaConfirmingInCorso()` [DUAL]
- `CHomeFactoring.getProcessiAperti()` [HF_PROCESSO]
- `CHomeFactoring.getProgCedutoEssere()` [DUAL]
- `CHomeFactoring.getProgFidoEssere()` [DUAL]
- `CHomeFactoring.insAllegati()` [TSEFCDOCPRATICA]
- `CHomeFactoring.insAnagrafica()` [DUAL,HF_ANAGRAFE,TSEFAANAG]
- `CHomeFactoring.insCollegatiSemplificati()` [HF_COLLEGATI]
- `CHomeFactoring.insDatiRiassicurazione()` [HF_RIASSICURAZIONE]
- `CHomeFactoring.insDatiRiassicurazioneCoppia()` [HF_RIASSICURAZIONE_RAPPORTO]
- `CHomeFactoring.insPraticaFornitore()` [TSEFCONDPRATICA]
- `CHomeFactoring.insPraticaRAC()` [TSEFAPRAT]
- `CHomeFactoring.insProcesso()` [DUAL,HF_PROCESSO]
- `CHomeFactoring.insRichiesta()` [HF_RICHIESTA]
- `CHomeFactoring.isCedenteFornitore()` [DUAL]
- `CHomeFactoring.isCedenteValido()` [DUAL]
- `CHomeFactoring.isCoppiaFactoring()` [DUAL]
- `CHomeFactoring.isNdgCedenteInProposta()` [DUAL]
- `CHomeFactoring.isNdgCedenteValido()` [DUAL]
- `CHomeFactoring.leggiCab()`[TSEFACOMUNECAB]
- `CHomeFactoring.leggiNdgEsistenteAnagFoW()` [TSEFAANAG]
- `CHomeFactoring.leggiSequenceRichiesta()` [DUAL]
- `CHomeFactoring.leggiStatoPratica()` [HF_STATO_PRATICA]
- `CHomeFactoring.updateAnagrafica()` [HF_ANAGRAFE]
- `CHomeFactoring.valAbiCabOK()` [TSEFBANCHE_SPORTELLI]
- `CHomeFactoring.valAtecoOK()` [TSEFATECO]
- `CHomeFactoring.valCondizioneOK()` [TSEFVALCONDIZIONI]
- `CHomeFactoring.valDivisaOK()` [TSEFDIVISE]
- `CHomeFactoring.valFormaGiuridicaOK()` [TSEFCFORMASOCIALE]
- `CHomeFactoring.valProvinciaOK()` [TSEFACOMUNECAB]
- `CHomeFactoring.valStatoOK()` [TSEFANAZIONI]

### CPlanner

- `CPlanner.assegnaPratiche()` [TSEFAPRATICA,TSEFAANAG]
- `CPlanner.assegnaPratiche()` [TSEFAPRATICA,TSEFAANAG]

### CProvider

- `CProvider.InsertRich()` [DUAL,TIPFRICH,TIPFREQ]
- `CProvider.getCodiceFiscale()` [TSEFAANAG]
- `CProvider.getRichieste()` [TIPFARICH,TIPFRICH,TIPFREQ]

### DBCom

- `DBCom.getParametroConf()` [TSEFCONF]

### Oggetti DE

- COMMRIF
- DOM_ID_ESTERO
- DUAL
- HF_ANAGRAFE
- HF_COLLEGATI
- HF_PROCESSO
- HF_RIASSICURAZIONE
- HF_RIASSICURAZIONE_RAPPORTO
- HF_RICHIESTA
- HF_STATO_PRATICA
- IS_ANAG_OK_PER_CR_REQ()
- SNDPRA0F
- TIPFARICH
- TIPFREQ
- TIPFRICH
- TSEFAANAG
- TSEFAANAG_DELIB
- TSEFAANAG_ID_ESTERO
- TSEFACEDENTI
- TSEFACOMUNECAB
- TSEFACONTIPROSPETTI
- TSEFANAZIONI
- TSEFAPLURIMI
- TSEFAPRAT
- TSEFAPRATICA
- TSEFASTATI
- TSEFATECO
- TSEFAUTENTI
- TSEFBANCHE_SPORTELLI
- TSEFCCONTRCONF
- TSEFCDOCPRATICA
- TSEFCFORMASOCIALE
- TSEFCONDCONTI
- TSEFCONDCONTI_LOG
- TSEFCONDPRATICA
- TSEFCONF
- TSEFCONTI
- TSEFCONTICEDUTI
- TSEFDIVISE
- TSEFFIDICEDENTE
- TSEFFORMETECNICHE
- TSEFOPEPOOL_CONTI
- TSEFVALCONDIZIONI
