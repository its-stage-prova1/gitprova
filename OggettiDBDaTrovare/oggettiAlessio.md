### CCHromeFactoring

```C#
leggiSequenceRichiesta() 
{"K_HF_RICHIESTA"}
insRichiesta()
{"HF_RICHIESTA"}
aggRichiesta()
{"HF_RICHIESTA"}
esistePlafondValido()
{"PKG_UTILITA.existsPlafondValido(:banca,:fPdo,:cedExt,:ndgDeb,0)"}
leggiStatoPratica()
{"HF_STATO_PRATICA"}
leggiNdgEsistenteAnagFoW()
{"TSEFAANAG"}
insPraticaRAC()
{"TSEFAPRAT"}
insAllegati()
{"TSEFCDOCPRATICA"}
valStatoOK()
{"TSEFANAZIONI"}
valFormaGiuridicaOK()
{"TSEFCFORMASOCIALE"}
valCabOK()
{"TSEFACOMUNECAB"}
leggiCab()
{"TSEFACOMUNECAB"}
valProvinciaOK()
{"TSEFACOMUNECAB"}
valDivisaOK()
{"TSEFDIVISE"}
valAbiCabOK()
{"TSEFBANCHE_SPORTELLI"}
valAtecoOK()
{"TSEFATECO"}
valCondizioneOK()
{"tsefvalcondizioni"}
valContoEssereOK()
{"tsefapratica" join "tsefconti" join "tsefacontiprospetti"}
insProcesso()
{"K_HF_PROCESSO" , "HF_PROCESSO"} /*sequence con insert*/
getProcessiAperti()
{"HF_PROCESSO"}
```
### CCedenti

```C#
getCedenti()
{"TSEFACEDENTI"}
getCedenteProspect()
{"TSEFACEDENTI_PROSPECT", "TSEFACEDENTI"}
getCedentePerNDG()
{"TSEFACEDENTI" , "TSEFACEDENTI_PROSPECT"}
isCedenteProspectAttivato()
{"TSEFACEDENTI_PROSPECT C join TSEFAPRATICA P" , "SNDPRA0F"}
getAnagrafeCedenti_136()
{"tsefceduti inner join tsefacedenti" , "inner join tsefaanag"}
getAnagrafeCedentiArt53()
{"tsefceduti inner join tsefacedenti" , "inner join tsefaanag"}
```
### CAnagrafe

```C#
caricaFormeSociali()
{"TSEFCFORMASOCIALE"}
leggiAnagrafe()
{"TSEFAANAG" , "TSEFAANAG_DELIB"}
setAnagCompleta()
{"TSEFAANAG"}
getNdg()
{"TSEFAANAG"}
setAnagraficaDaInviare()
{"TSEFAANAG_VARIAZIONE"}
isRaeInseribile()
{"tsefsae"}
GetDominioIdEstero()
{"DOM_ID_ESTERO"}
salvaIdEstero()
{"TSEFAANAG_ID_ESTERO"}
setVat()
{"TSEFAANAG"}
updateAnagrafeEsteraDaWip()
{"TSEFAANAG"}
```
### DBCom

```C#
getParametroConf()
{"TSEFCONF"}
getParametroConfig()
{"TSEFCONFIG"}
```

### CPlanner

```C#
assegnaPratiche()
{"TSEFAPRATICA P, TSEFAANAG A"}
```

### CCeduti

```C#
updateRapportoInProposta()
{"tsefapratica p join tsefastati" , "tsefceduti c" , "TSEFCONF" }
esisteRapportoProspect()
{"HF_RAPP_PROSPECT_ELAB join HF_RAPP_PROSPECT"}
cedutoInProposta()
{"TSEFCEDUTI" , "TSEFCONDCEDUTI" , "TSEFCONTICEDUTI", "TSEFRISCHIORAPP" }
salvaCedutoConCedente()
{"TSEFAPRATICA" , "TSEFCEDUTI"}
```
